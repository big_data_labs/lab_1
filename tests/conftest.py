import pytest

from src.api.models import PredictRequest


@pytest.fixture()
def api_url() -> str:
    return "http://localhost:8000/predict"


@pytest.fixture()
def sample_input_data() -> PredictRequest:
    return PredictRequest(
        pregnancies=1,
        glucose=148,
        blood_pressure=72,
        skin_thickness=35,
        insulin=0,
        bmi=33.6,
        diabetes_pedigree_function=0.627,
        age=50
    )