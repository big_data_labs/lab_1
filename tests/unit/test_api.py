import pytest

from src.api.app import predict
from src.api.models import PredictResponse


@pytest.mark.asyncio
async def test_predict_api(sample_input_data):
    response = await predict(sample_input_data)
    assert isinstance(response, PredictResponse)
    assert isinstance(response.outcome, bool)
