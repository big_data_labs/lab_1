import pytest
import requests

from src.api.models import PredictResponse


@pytest.mark.skip
@pytest.mark.asyncio
async def test_predict_api(api_url, sample_input_data):
    response = requests.post(api_url, json=sample_input_data.dict())
    assert response.status_code == 200
    response_data = PredictResponse.parse_obj(response.json())
    assert isinstance(response_data.outcome, bool)
