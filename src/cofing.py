import os
from pathlib import Path

ROOT_DIR = Path(__file__).parent.parent
DATA_DIR = ROOT_DIR / "data"

LOGS_DIR = ROOT_DIR / "logs"
os.makedirs(LOGS_DIR, exist_ok=True)


n_neighbors = 10
