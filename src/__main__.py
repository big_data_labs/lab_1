import uvicorn
from loguru import logger

from src.cofing import LOGS_DIR

logger.add(LOGS_DIR / "logfile.log", rotation="10 MB", retention="10 days", level="DEBUG")


if __name__ == "__main__":
    logger.info("Start application")
    uvicorn.run("src.api.app:app", host="0.0.0.0", port=8000, log_level="info")
