import pandas as pd
from fastapi import FastAPI
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.openapi.utils import get_openapi
from loguru import logger
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import HTMLResponse, JSONResponse

from src import cofing
from src.api.models import PredictRequest, PredictResponse
from src.ml.KNNClassificator import KNNClassificator

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

model = KNNClassificator(cofing.DATA_DIR / "diabetes.csv")


@app.get("/ping")
def read_root():
    return {'Ans': "pong"}


@app.post("/predict")
async def predict(request: PredictRequest) -> PredictResponse:
    logger.info("Get new request")
    df = pd.DataFrame({
        'Pregnancies': [request.pregnancies],
        'Glucose': [request.glucose],
        'BloodPressure': [request.glucose],
        'SkinThickness': [request.skin_thickness],
        'Insulin': [request.insulin],
        'BMI': [request.bmi],
        'DiabetesPedigreeFunction': [request.diabetes_pedigree_function],
        'Age':[request.age]
    })
    result = model.predict(df)
    logger.info("Predict result %s" % result)
    return PredictResponse(outcome=result[0])


@app.get("/docs", response_class=HTMLResponse)
async def custom_swagger_ui_html():
    return get_swagger_ui_html(openapi_url="/openapi.json", title="API docs")


@app.get("/openapi.json", include_in_schema=False)
async def get_open_api_endpoint():
    return JSONResponse(get_openapi(title="Your Project Name", version="0.1.0", routes=app.routes))
